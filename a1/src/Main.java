import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String[] newArray = new String[5];
        newArray[0] = "apple";
        newArray[1] = "orange";
        newArray[2] = "avocado";
        newArray[3] = "kiwi";
        newArray[4] = "lemon";

        System.out.println(Arrays.toString(newArray));

        Scanner scannerName = new Scanner(System.in);

        System.out.println("Which fruit would you like to get the index of?");

        String fruitName = scannerName.nextLine();


        String searchTerm = "lemon";
        int result = Arrays.binarySearch(newArray,searchTerm);
        System.out.println("The index of " + searchTerm + " is: " + result);

        ArrayList<String> friendList = new ArrayList<>();

        friendList.add("Erika");
        friendList.add("Nina");
        friendList.add("Pia");
        friendList.add("Dylan");

        System.out.println("My friends are: " + friendList);

        HashMap<String,String> itemList = new HashMap<>();
        itemList.put("Notebook","60");
        itemList.put("Bag", "40");
        itemList.put("Pencil", "10");
        System.out.println("Our current inventory consist of: "  + itemList);









    }
}